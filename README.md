# Hiberus challenge

<p align="center">
  <a href="https://ionicframework.com/" target="blank">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Ionic_Logo.svg/1024px-Ionic_Logo.svg.png?20180201214056" width="300" alt="Ionic Logo" /></a>
</p>

### Requirements

- node ^14.0.0

### Install dependencies

```sh
npm i -g @angular/cli
npm i -g @ionic/cli
npm i
```

### Running the app

```bash
ionic serve
```