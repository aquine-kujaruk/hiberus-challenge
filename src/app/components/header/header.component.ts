import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() checkedUsers: any;
  @Output() searchChange = new EventEmitter();
  @Output() deleteUsers = new EventEmitter();
  @Output() logout = new EventEmitter();

  loading: boolean;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select('ui').subscribe((state) => {
      this.loading = state.loading;
    });
  }

  onDeleteUsers() {
    this.deleteUsers.emit();
  }

  onSearchChange(event: any) {
    this.searchChange.emit(event.detail.value);
  }

  async onLogout() {
    this.logout.emit();
  }
}
