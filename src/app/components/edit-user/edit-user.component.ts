import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { updateUser } from '../../store/actions';
import { User } from '../../interfaces/user.interface';
import { AppState } from '../../store/app.reducers';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  @Input() user: User = { name: '', surname: '', email: '', id: '' };

  constructor(private store: Store<AppState>, private uiService: UiService) {}

  ngOnInit() {}

  async onSubmit() {
    const handler = () => this.store.dispatch(updateUser({ user: this.user }));
    const action = 'edit user';
    await this.uiService.showConfirmDialog(handler, action);
  }
}
