import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';
import { User } from '../../interfaces/user.interface';
import { selectUser } from '../../store/actions';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss'],
})
export class DrawerComponent implements OnInit {
  user: User = null;

  constructor(
    private menuController: MenuController,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.store.select('user').subscribe((state) => {
      this.user = { ...state.user };
    });
  }

  closeMenu() {
    this.menuController.close('drawer');
  }

  onClose() {
    this.store.dispatch(selectUser({ user: null }));
  }
}
