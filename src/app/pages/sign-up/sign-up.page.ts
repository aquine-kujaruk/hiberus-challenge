import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../interfaces/user.interface';
import { AppState } from '../../store/app.reducers';
import { Store } from '@ngrx/store';
import { signupStart } from '../../store/actions/auth.action';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  user = {} as User;
  loading: boolean;

  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select('ui').subscribe((state) => {
      this.loading = state.loading;
    });
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(signupStart({ credentials: { ...this.user } }));
    this.user = form.value;
  }

  goToLogin() {
    this.router.navigateByUrl('/login', { replaceUrl: true });
  }
}
