import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';
import { loginStart } from '../../store/actions';
import { User } from '../../interfaces/user.interface';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user = {} as User;
  loading: boolean;

  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select('ui').subscribe((state) => {
      this.loading = state.loading;
    });
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(loginStart({ credentials: { ...this.user } }));
    this.user = form.value;
  }

  goToSignUp() {
    this.router.navigateByUrl('/sign-up', { replaceUrl: true });
  }
}
