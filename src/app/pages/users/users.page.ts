import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { deleteUsers, getUsers, logout, selectUser } from '../../store/actions';
import { AppState } from '../../store/app.reducers';
import { UiService } from '../../services/ui.service';
import { AuthResponse } from '../../interfaces/auth-response.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  authInfo: AuthResponse;
  searchText: string;
  checkedUsers: string[] = [];

  users: User[] = [];

  constructor(
    private menuController: MenuController,
    private store: Store<AppState>,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.store.select('users').subscribe((state) => {
      this.users = state.users;
    });

    this.store.select('auth').subscribe((state) => {
      this.authInfo = state.authInfo;
    });

    this.store.dispatch(getUsers());
  }

  onSearchChange(searchText: string) {
    this.searchText = searchText;
  }

  async onDeleteUsers() {
    const handler = () => {
      this.store.dispatch(
        deleteUsers({ ids: [...new Set(this.checkedUsers)] })
      );
      this.checkedUsers = [];
    };
    const action = 'remove selected users';
    await this.uiService.showConfirmDialog(handler, action);
  }

  onCheckUser(event: any) {
    const id = event.target.id;

    if (event.target.checked) {
      this.checkedUsers.push(id);
    } else {
      this.checkedUsers = this.checkedUsers.filter((user) => user !== id);
    }
  }

  editUser(user: User) {
    this.store.dispatch(selectUser({ user }));
    this.menuController.open('drawer');
  }

  async logout() {
    const handler = () => this.store.dispatch(logout());
    const action = 'logout';
    await this.uiService.showConfirmDialog(handler, action);
  }
}
