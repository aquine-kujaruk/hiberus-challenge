import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { UsersPageRoutingModule } from './users-routing.module';
import { UsersPage } from './users.page';

@NgModule({
  imports: [
  CommonModule,
    IonicModule,
    UsersPageRoutingModule,
    ComponentsModule,
    PipesModule,
  ],
  declarations: [UsersPage],
})
export class UsersPageModule {}
