/* eslint-disable @typescript-eslint/naming-convention */
export enum UiTypes {
  SET_LOADING = '[Ui] Set Loading',
  SET_ERROR = '[Ui] Set Error Message',
  SET_NOTICE = '[Ui] Set Notice Message',
}
