/* eslint-disable @typescript-eslint/naming-convention */
export enum AuthTypes {
  LOGIN_START = '[Auth] Login start',
  LOGIN_SUCCESS = '[Auth] Login Success',

  SIGNUP_START = '[Auth] Signup start',
  SIGNUP_SUCCESS = '[Auth] Signup success',

  AUTO_LOGIN_ACTION = '[Auth] Auto Login',
  LOGOUT_ACTION = '[Auth] Logout',
}
