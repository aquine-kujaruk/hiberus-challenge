/* eslint-disable @typescript-eslint/naming-convention */
export enum UsersTypes {
  GET_USERS = '[Users] Get Users',
  GET_USER_SUCCESS = '[Users] Get Users Success',
  GET_USERS_ERROR = '[Users] Get Users Error',
}
