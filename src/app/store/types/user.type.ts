/* eslint-disable @typescript-eslint/naming-convention */
export enum UserTypes {
  SELECT_USER = '[User] Select User',

  UPDATE_USER = '[User] Update User',
  UPDATE_USER_SUCCESS = '[User] Update User Success',
  UPDATE_USER_ERROR = '[User] Update User Error',

  DELETE_USER = '[User] Delete User',
  DELETE_USER_SUCCESS = '[User] Delete User Success',
  DELETE_USER_ERROR = '[User] Delete User Error',
}
