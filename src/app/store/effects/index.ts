import { UsersEffects } from './users.effect';
import { UserEffects } from './user.effect';
import { AuthEffects } from './auth.effect';
import { UiEffects } from './ui.effect';

export const appEffects: any[] = [
  UsersEffects,
  UserEffects,
  AuthEffects,
  UiEffects,
];
