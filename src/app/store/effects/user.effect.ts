import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap, tap } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import {
  deleteUsers,
  deleteUsersSuccess,
  getUsers,
  updateUser,
  updateUserSuccess,
  setLoading,
  setError,
  setNotice,
} from '../actions';
import { AppState } from '../app.reducers';

@Injectable()
export class UserEffects {
  updateUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateUser),
      exhaustMap((action) =>
        this.userService.updateUser(action.user).pipe(
          map(() => {
            this.store.dispatch(setLoading({ loading: true }));
            return updateUserSuccess();
          }),
          catchError(({ error }) => {
            this.store.dispatch(setLoading({ loading: false }));
            return of(setError({ payload: error }));
          })
        )
      )
    )
  );

  updateUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateUserSuccess),
        tap(() => {
          this.store.dispatch(setError({ payload: null }));
          this.store.dispatch(
            setNotice({
              payload: {
                message: 'User successfully updated',
                color: 'success',
              },
            })
          );
          this.store.dispatch(getUsers());
          this.menuController.close('drawer');
        })
      ),
    { dispatch: false }
  );

  deleteUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteUsers),
      mergeMap((action) =>
        this.userService.deleteUsers(action.ids).pipe(
          map(() => {
            this.store.dispatch(setLoading({ loading: true }));
            return deleteUsersSuccess();
          }),
          catchError(({ error }) => {
            this.store.dispatch(setLoading({ loading: false }));
            return of(setError({ payload: error }));
          })
        )
      )
    )
  );

  deleteUsersSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteUsersSuccess),
        tap(() => {
          this.store.dispatch(setError({ payload: null }));
          this.store.dispatch(
            setNotice({
              payload: {
                message: 'Users successfully deleted',
                color: 'success',
              },
            })
          );
          this.store.dispatch(getUsers());
          this.menuController.close('drawer');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private menuController: MenuController,
    private store: Store<AppState>
  ) {}
}
