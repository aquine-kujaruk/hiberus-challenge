import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap, tap } from 'rxjs/operators';
import {
  autoLogin,
  logout,
  getUsersSuccess,
  loginStart,
  loginSuccess,
  setError,
  setLoading,
  signupStart,
  signupSuccess,
  setNotice,
} from '../actions';
import { AppState } from '../app.reducers';
import { AuthService } from './../../services/auth.service';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginStart),
      exhaustMap(({ credentials }) =>
        this.authService.login(credentials).pipe(
          map((authInfo) => {
            if (authInfo) {
              this.authService.runTimeoutInterval(authInfo);
            }
            this.store.dispatch(setLoading({ loading: true }));
            return loginSuccess({ authInfo });
          }),
          catchError(({ error }) => {
            this.store.dispatch(setLoading({ loading: false }));
            return of(setError({ payload: error ?? 'Error extraño' }));
          })
        )
      )
    )
  );

  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccess),
        tap((action) => {
          this.store.dispatch(setLoading({ loading: false }));
          this.store.dispatch(setError({ payload: null }));
          this.router.navigateByUrl('/', { replaceUrl: true });
        })
      ),
    { dispatch: false }
  );

  signUp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(signupStart),
      exhaustMap(({ credentials }) =>
        this.authService.signUp(credentials).pipe(
          map(() => {
            this.store.dispatch(setLoading({ loading: true }));
            return signupSuccess();
          }),
          catchError(({ error }) => {
            this.store.dispatch(setLoading({ loading: false }));
            return of(setError({ payload: error }));
          })
        )
      )
    )
  );

  signUpRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(signupSuccess),
        tap(() => {
          this.store.dispatch(setLoading({ loading: false }));
          this.store.dispatch(setError({ payload: null }));
          this.store.dispatch(
            setNotice({
              payload: {
                message: 'User successfully created, please log in',
                color: 'success',
              },
            })
          );
          this.router.navigateByUrl('/login', { replaceUrl: true });
        })
      ),
    { dispatch: false }
  );

  autoLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(autoLogin),
      mergeMap(() => {
        const authInfo = this.authService.getAuthInfoFromLocalStorage();

        if (authInfo) {
          this.authService.runTimeoutInterval(authInfo);
        }

        return of(loginSuccess({ authInfo }));
      })
    )
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logout),
        map(() => {
          this.authService.logout();
          this.store.dispatch(getUsersSuccess({ users: [] }));
          this.router.navigateByUrl('/login', { replaceUrl: true });
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
  ) {}
}
