import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { setError, setNotice } from '../actions';

@Injectable()
export class UiEffects {
  errorNotification$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setError),
        map(async ({ payload }) => {
          if (payload) {
            const toast = await this.toastController.create({
              message:
                payload?.message ?? 'Unknown error occurred. Please try again',
              duration: 2000,
              color: 'danger',
              position: 'bottom',
            });
            toast.present();
          }
        })
      ),
    { dispatch: false }
  );

  setNotice$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setNotice),
        map(async ({ payload }) => {
          const toast = await this.toastController.create({
            message: payload.message,
            duration: 2000,
            color: payload.color,
            position: 'bottom',
          });
          toast.present();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private toastController: ToastController
  ) {}
}
