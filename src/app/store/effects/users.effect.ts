import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { getUsers, getUsersSuccess, setError, setLoading } from '../actions';
import { AppState } from '../app.reducers';

@Injectable()
export class UsersEffects {
  getUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUsers),
      exhaustMap(() =>
        this.userService.getUsers().pipe(
          map((users) => {
            this.store.dispatch(setLoading({ loading: true }));
            return getUsersSuccess({ users });
          }),
          catchError((err) => {
            this.store.dispatch(setLoading({ loading: false }));
            return of(setError({ payload: err }));
          })
        )
      )
    )
  );

  getUsersSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUsersSuccess),
      map(() => setLoading({ loading: false }))
    )
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private store: Store<AppState>
  ) {}
}
