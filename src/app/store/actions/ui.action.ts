import { createAction, props } from '@ngrx/store';
import { UiTypes } from '../types';

export const setLoading = createAction(
  UiTypes.SET_LOADING,
  props<{ loading: boolean }>()
);

export const setError = createAction(
  UiTypes.SET_ERROR,
  props<{ payload: any }>()
);

export const setNotice = createAction(
  UiTypes.SET_NOTICE,
  props<{ payload: any }>()
);
