import { createAction, props } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { UsersTypes } from '../types';

export const getUsers = createAction(UsersTypes.GET_USERS);

export const getUsersSuccess = createAction(
  UsersTypes.GET_USER_SUCCESS,
  props<{ users: User[] }>()
);
