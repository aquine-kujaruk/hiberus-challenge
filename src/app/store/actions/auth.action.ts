import { createAction, props } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { AuthTypes } from '../types';
import { AuthResponse } from '../../interfaces/auth-response.interface';

export const loginStart = createAction(
  AuthTypes.LOGIN_START,
  props<{ credentials: Partial<User> }>()
);
export const loginSuccess = createAction(
  AuthTypes.LOGIN_SUCCESS,
  props<{ authInfo: AuthResponse }>()
);

export const signupStart = createAction(
  AuthTypes.SIGNUP_START,
  props<{ credentials: Partial<User> }>()
);

export const signupSuccess = createAction(AuthTypes.SIGNUP_SUCCESS);

export const autoLogin = createAction(AuthTypes.AUTO_LOGIN_ACTION);

export const logout = createAction(AuthTypes.LOGOUT_ACTION);
