import { createAction, props } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { UserTypes } from '../types';

export const selectUser = createAction(
  UserTypes.SELECT_USER,
  props<{ user: User }>()
);

export const updateUser = createAction(
  UserTypes.UPDATE_USER,
  props<{ user: User }>()
);

export const updateUserSuccess = createAction(UserTypes.UPDATE_USER_SUCCESS);

export const deleteUsers = createAction(
  UserTypes.DELETE_USER,
  props<{ ids: string[] }>()
);

export const deleteUsersSuccess = createAction(UserTypes.DELETE_USER_SUCCESS);
