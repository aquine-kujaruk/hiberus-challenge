import { ActionReducerMap } from '@ngrx/store';
import * as reducers from './reducers';

export interface AppState {
  ui: reducers.UiState;
  auth: reducers.AuthState;
  user: reducers.UserState;
  users: reducers.UsersState;
}

export const appReducers: ActionReducerMap<AppState> = {
  ui: reducers.uiReducer,
  auth: reducers.authReducer,
  user: reducers.userReducer,
  users: reducers.usersReducer,
};
