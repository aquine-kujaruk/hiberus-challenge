/* eslint-disable no-underscore-dangle */
import { Action, createReducer, on } from '@ngrx/store';
import { setLoading, setError } from '../actions';

export interface UiState {
  loading: boolean;
  error: any;
}

const initialState: UiState = {
  loading: false,
  error: {},
};

const _uiReducer = createReducer(
  initialState,

  on(setLoading, (state, { loading }) => ({
    ...state,
    loading,
  })),

  on(setError, (state, { payload }) => ({
    ...state,
    error: {
      url: payload?.url,
      name: payload?.name,
      message: payload?.message,
    },
  }))
);

export const uiReducer = (state: UiState, action: Action) =>
  _uiReducer(state, action);
