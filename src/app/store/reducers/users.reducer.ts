/* eslint-disable no-underscore-dangle */
import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { getUsersSuccess } from '../actions';

export interface UsersState {
  users: User[];
}

const initialState: UsersState = {
  users: [],
};

const _usersReducer = createReducer(
  initialState,

  on(getUsersSuccess, (state, { users }) => ({
    ...state,
    users,
  }))
);

export const usersReducer = (state: UsersState, action: Action) =>
  _usersReducer(state, action);
