/* eslint-disable no-underscore-dangle */
import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../../interfaces/user.interface';
import { selectUser } from '../actions';

export interface UserState {
  user: User;
}

const initialState: UserState = {
  user: null,
};

const _userReducer = createReducer(
  initialState,

  on(selectUser, (state, { user }) => ({
    ...state,
    user,
  }))
);

export const userReducer = (state: UserState, action: Action) =>
  _userReducer(state, action);
