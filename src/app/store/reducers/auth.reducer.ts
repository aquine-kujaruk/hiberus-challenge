/* eslint-disable no-underscore-dangle */
import { Action, createReducer, on } from '@ngrx/store';
import { loginSuccess, logout } from '../actions';
import { AuthResponse } from '../../interfaces/auth-response.interface';

export interface AuthState {
  authInfo: AuthResponse;
}

const initialState: AuthState = {
  authInfo: null,
};

const _authReducer = createReducer(
  initialState,

  on(loginSuccess, (state, { authInfo }) => ({
    ...state,
    authInfo,
  })),

  on(logout, (state) => ({
    ...state,
    authInfo: null,
  }))
);

export const authReducer = (state: AuthState, action: Action) =>
  _authReducer(state, action);
