import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor(private alertController: AlertController) {}

  async showConfirmDialog(handler: () => void, action: string) {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: `You shure, that you want to ${action}?`,
      buttons: [
        { text: 'Cancel', role: 'cancel' },
        { text: 'Ok', handler },
      ],
    });

    await alert.present();
  }
}
