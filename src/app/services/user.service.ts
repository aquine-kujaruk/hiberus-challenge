/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../interfaces/user.interface';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private url = environment.BASE_URL;

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(`${this.url}/users`).pipe(map(this.sortUsers));
  }

  updateUser({ id, name, surname }: Partial<User>) {
    return this.http.put(`${this.url}/users/${id}`, { name, surname });
  }

  deleteUsers(ids: string[]) {
    return forkJoin(
      ids.map((id) => this.http.delete(`${this.url}/users/${id}`))
    );
  }

  private sortUsers({ items }: { items: User[] }) {
    return items.sort((a: any, b: any) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
  }
}
