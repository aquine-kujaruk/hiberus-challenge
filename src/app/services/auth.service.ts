/* eslint-disable @typescript-eslint/member-ordering */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthResponse } from '../interfaces/auth-response.interface';
import { User } from '../interfaces/user.interface';
import { AppState } from '../store/app.reducers';
import { environment } from './../../environments/environment';
import { logout } from '../store/actions/auth.action';
import { map } from 'rxjs/operators';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  timeoutInterval: any;
  private url = environment.BASE_URL;

  constructor(private http: HttpClient, private store: Store<AppState>) {}

  login(credentials: Partial<User>): Observable<AuthResponse> {
    return this.http
      .post<AuthResponse>(`${this.url}/auth/log-in`, credentials)
      .pipe(map(this.processAuthInfo));
  }

  signUp(credentials: Partial<User>): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(
      `${this.url}/auth/sign-up`,
      credentials
    );
  }

  getAuthInfoFromLocalStorage() {
    const authInfoString = localStorage.getItem('authInfo');
    if (authInfoString) {
      const authInfo = JSON.parse(authInfoString);
      return authInfo;
    }
    return null;
  }

  logout() {
    localStorage.removeItem('authInfo');
    if (this.timeoutInterval) {
      clearTimeout(this.timeoutInterval);
      this.timeoutInterval = null;
    }
  }

  runTimeoutInterval(authInfo: AuthResponse) {
    const todaysDate = new Date().getTime();
    const expirationDate = authInfo.exp * 1000;
    const timeInterval = expirationDate - todaysDate;

    return (this.timeoutInterval = setTimeout(() => {
      this.store.dispatch(logout());
      //logout functionality or get the refresh token
    }, timeInterval));
  }

  private processAuthInfo(authInfo: AuthResponse) {
    const processedAuthInfo = {
      ...authInfo,
      ...(jwtDecode(authInfo.accessToken) as Record<string, any>),
    };

    localStorage.setItem('authInfo', JSON.stringify(processedAuthInfo));

    return processedAuthInfo;
  }
}
