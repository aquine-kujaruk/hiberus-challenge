import { User } from './user.interface';

export interface AuthResponse {
  accessToken: string;
  refreshToken: string;
  tokenType: string;
  date: string;
  iat: number;
  exp: number;
  id: string;
}
