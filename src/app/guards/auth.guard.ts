import { exhaustMap, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppState } from '../store/app.reducers';
import { CanLoad, Router } from '@angular/router';
import { isAuthenticated } from '../store/selectors/auth.selector';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanLoad {
  constructor(private store: Store<AppState>, private router: Router) {}

  canLoad() {
    return this.store.select(isAuthenticated).pipe(
      map((authenticate) => {
        if (!authenticate) {
          return this.router.createUrlTree(['login']);
        }
        return true;
      })
    );
  }
}
