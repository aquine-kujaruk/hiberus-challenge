import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarImagePipe } from './avatar-image.pipe';
import { SearchFilterPipe } from './search-filter.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [AvatarImagePipe, SearchFilterPipe],
  exports: [AvatarImagePipe, SearchFilterPipe],
})
export class PipesModule {}
