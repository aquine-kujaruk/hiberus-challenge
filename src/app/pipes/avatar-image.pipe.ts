import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'avatarImage',
})
export class AvatarImagePipe implements PipeTransform {
  private domain = 'https://ui-avatars.com/api/';

  transform(value: { name: string; surname: string }): string {
    const fullname = `${value.name} ${value.surname}`;

    return `${this.domain}?name=${fullname}&size=400&rounded=true&background=383a3e0&color=d7d8da&uppercase=true`;
  }
}
