import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter',
})
export class SearchFilterPipe implements PipeTransform {
  transform(data: any[], searchText: string = ''): any[] {
    if (searchText === '' || !data) {
      return data;
    }

    searchText = searchText.toLocaleLowerCase();

    const itemsWithSearcheableText = data.map((item) => {
      const searcheableText = [];

      for (const key in item) {
        if (typeof item[key] === 'string') {
          searcheableText.push(item[key].toLocaleLowerCase().trim());
        }
      }

      return { ...item, searcheableText: searcheableText.join(' ') };
    });

    return itemsWithSearcheableText.filter((item) =>
      item.searcheableText.includes(searchText)
    );
  }
}
